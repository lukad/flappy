local class = require('lib/middleclass')
local vector = require('lib/hump/vector')
local Timer = require('lib/hump/timer')

local Bird = class('Bird')

Bird.static.sprite = love.graphics.newImage('assets/bird.png')
Bird.static.jumpSound = love.audio.newSource('assets/jump.wav', 'static')

function Bird:initialize()
  self.timer = Timer.new()
  self.pos = vector(100, 245)
  self.delta = vector(0, 0)
  self.gravity = vector(0, 1000)
  self.width = 50
  self.height = 50
  self.angle = 0
  self.dead = false
end

function Bird:update(dt)
  self.delta = self.delta + self.gravity * dt
  self.pos = self.pos + self.delta * dt
  if self.dead then return end
  self.angle = self.angle + 50 * dt
  self.timer.update(dt)
end

function Bird:draw()
  love.graphics.draw(Bird.static.sprite, self.pos.x, self.pos.y, math.rad(self.angle))
end

function Bird:jump()
  if self.dead then return end
  self.delta.y = -350
  self.timer.tween(0.1, self, {angle = -20})
  love.audio.play(Bird.static.jumpSound)
end

function Bird:kill()
  self.dead = true
end

return Bird
