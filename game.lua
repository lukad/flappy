local Bird = require('bird')
local Pipe = require('pipe')
local Timer = require('lib/hump/timer')
local Gamestate = require('lib/hump/gamestate')
local Gameover = require('gameover')

local Game = {}

function Game:enter()
  Game.timer = Timer.new()
  Game.score = 0
  love.graphics.setBackgroundColor(113, 197, 207)
  Game.bird = Bird:new()
  Game.pipes = {}

  Game.timer.after(1.5, function()
		     addPipes()
		     Game.timer.every(1.5, function() addPipes() end)
		     Game.timer.every(1.5, function() Game.score = Game.score + 1 end)
  end)
end

function addPipes()
  local gap = math.floor(love.math.random(1, 5))
  for i = 0, 7 do
    if i == gap or i == gap + 1 then goto continue end
    table.insert(Game.pipes, Pipe:new(400, i * 60 + 10))
    ::continue::
  end
end

function checkCollision(a_x, a_y, a_width, a_height, b_x, b_y, b_width, b_height)
  return a_x < b_x + b_width and
    a_x + a_width > b_x and
    a_y < b_y + b_height and
    a_height + a_y > b_y
end

function death()
  if Game.bird.dead then return end
  for _, pipe in pairs(Game.pipes) do
    pipe.delta.x = 0
  end
  Game.bird:kill()
  Game.timer.clear()
  Game.timer.after(1, function() Gamestate.switch(Gameover, Game.score) end)
end

function Game:update(dt)
  Game.timer.update(dt)

  if Game.bird.pos.y < 0 or Game.bird.pos.y > 490 - 50 then
    death()
  end

  for _, pipe in pairs(Game.pipes) do
    if checkCollision(Game.bird.pos.x, Game.bird.pos.y, Game.bird.width, Game.bird.height,
		      pipe.pos.x, pipe.pos.y, pipe.width, pipe.height) then
      death()
    end
  end

  Game.bird:update(dt)

  for i = #Game.pipes, 1, -1 do
    Game.pipes[i]:update(dt)

    if Game.pipes[i].pos.x <= -50 then
      table.remove(Game.pipes, i)
    end
  end
end

function Game:draw()
  for _, pipe in pairs(Game.pipes) do
    pipe:draw()
  end
  Game.bird:draw()
  love.graphics.print('Score: ' .. Game.score, 0, 0)
end

function Game:keypressed(key, _scancode, _isrepeat)
  if key == 'space' then
    Game.bird:jump()
  elseif key == 'escape' then
    Gamestate.pop()
  end
end

return Game
