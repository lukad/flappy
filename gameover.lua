local Gamestate = require('lib/hump/gamestate')

local Gameover = {}

function Gameover:enter(game, score)
  Gameover.score = score
  Gameover.game = game
  love.graphics.setBackgroundColor(113, 197, 207)
end

function Gameover:draw()
  love.graphics.print('Game Over', 0, 0)
  love.graphics.print('Score: ' .. Gameover.score, 0, 20)
end

function Gameover:keypressed(key, _scancode, _isrepeat)
  if key == 'space' then
    Gamestate.switch(Gameover.game)
  end
end

return Gameover
