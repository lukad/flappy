local menu = require('menu')

local Gamestate = require('lib/hump/gamestate')

function love.keypressed(key)
  if Gamestate.current() ~= menu and key == 'escape' then
    Gamestate.switch(menu)
  end
end

function love.load()
  Gamestate.registerEvents()
  Gamestate.switch(menu)
end
