local Gamestate = require('lib/hump/gamestate')
local Game = require('game')

local Menu = {}

function Menu:enter(blyat)
  love.graphics.setBackgroundColor(113, 197, 207)
end

function Menu:draw()
  love.graphics.print('Menu', 0, 0)
end

function Menu:keypressed(key, _scancode, _isrepeat)
  if key == 'space' then
    Gamestate.push(Game)
  elseif key == 'escape' then
    love.event.quit()
  end
end

return Menu
