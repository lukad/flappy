local class = require('lib/middleclass')
local vector = require('lib/hump/vector')

local Pipe = class('Pipe')

function Pipe:initialize(x, y)
  self.pos = vector(x, y)
  self.delta = vector(-200, 0)
  self.width = 50
  self.height = 50
end

Pipe.static.sprite = love.graphics.newImage('assets/pipe.png')

function Pipe:draw()
  love.graphics.draw(Pipe.static.sprite, self.pos.x, self.pos.y)
end

function Pipe:update(dt)
  self.pos = self.pos + self.delta * dt
end

return Pipe
